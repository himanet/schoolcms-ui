
const cache = {
  saveCache(key,item){
    window.localStorage.setItem(key,JSON.stringify(item))
  },
  readCache(key){
    return window.localStorage.getItem(key)
  },
  removeCache(key){
    window.localStorage.setItem(key,'')
  }
}
export default cache