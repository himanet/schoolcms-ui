import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import './styles/element-variables.scss'
//import enLang from 'element-ui/lib/locale/lang/en'// 如果使用中文语言包请默认支持，无需额外引入，请删除该依赖

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'




import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log

import * as filters from './filters' // global filters
import moment from 'moment'


import 'xe-utils'
import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'
// import 'lib-flexible/flexible.js'

Vue.use(VXETable)

//引入全局挂载方法
import { getNowFormatDate,getCurrentTimeType,resetForm, addDateRange, selectDictLabel,handleTree ,getAge,GetAgeById,formatDateNew} from '@/utils/cms-utils'
import { getDataByType ,getDataByKey} from '@/api/system/dict/data'
//Vue挂载全局方法
Vue.prototype.moment = moment
Vue.prototype.getNowFormatDate = getNowFormatDate
Vue.prototype.getCurrentTimeType = getCurrentTimeType
Vue.prototype.resetForm = resetForm
Vue.prototype.addDateRange = addDateRange
Vue.prototype.selectDictLabel = selectDictLabel
Vue.prototype.getDataByType = getDataByType
Vue.prototype.getDataByKey = getDataByKey
Vue.prototype.handleTree = handleTree
Vue.prototype.getAge = getAge
Vue.prototype.GetAgeById = GetAgeById
Vue.prototype.formatDateNew = formatDateNew
//挂载全局消息框
Vue.prototype.msgSuccess = function(msg){
  this.$message({showClose:true , message:msg ,type:'success'})
}
Vue.prototype.msgError = function(msg){
  this.$message({showClose:true , message:msg ,type:'error'})
}
Vue.prototype.msgInfo = function(msg){
  this.$message.info(msg)
}

// element弹框工具
import dialogUtil from "@/utils/schoolUtils/dialogUtil"
Vue.component('dialogUtil', dialogUtil)

Vue.use(Element, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
  //locale: enLang // 如果使用中文，无需设置，请删除
})

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
