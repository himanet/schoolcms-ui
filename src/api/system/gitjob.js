
import request from '@/utils/request'

/**
 * 分页查询所有部门信息
 * @param {*} query
 * @returns
 */
export function listGitJobForPage(query){
  return request({
    url:`/system/getjob/listGitJobForPage`,
    method:'get',
    params:query
  })
}

/**
 * 添加就业信息
 * @param data
 */
export function addGetJob(data){
  return request({
    url: '/system/getjob/addGetJob',
    data:data,
    method:'post'
  })
}

/**
 * 修改就业信息
 * @param data
 * @returns {AxiosPromise}
 */
export function updateGetJob(data){
  return request({
    url: '/system/getjob/updateGetJob',
    data:data,
    method:'post'
  })
}

/**
 * 查询单个就业信息
 * @param jobId
 */
export function getJobById(jobId){
  return request({
    url: '/system/getjob/getJobById/'+jobId,
    method:'get'
  })
}
