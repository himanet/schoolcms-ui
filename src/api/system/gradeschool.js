import request from '@/utils/request'
const SERVICE_NAME = "ihis-system"

export function listForPage(query){
  return request({
    url:`/system/gradeschool/listForPage`,
    method:'get',
    params:query
  })
}


export function listForPageByRate(query){
  return request({
    url:`/system/gradeschool/listForPageByRate`,
    method:'get',
    params:query
  })
}

export function queryEnrollmentRate(classId){
  return request({
    url:`/system/gradeschool/queryEnrollmentRate/${classId}`,
    method:'get',
  })
}
