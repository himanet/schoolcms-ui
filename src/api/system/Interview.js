import request from '@/utils/request'
const SERVICE_NAME = "ihis-system"

export function listForPage(query){
  return request({
    url:`/system/Interview/listForPage`,
    method:'get',
    params:query
  })
}
export function addInterview(data){
  return request({
    url:`/system/Interview/addInterview`,
    method:'post',
    data
  })
}

/**
 * 根据学生id获取访谈记录
 * @param id
 * @returns {AxiosPromise}
 */
export function getInterviewById(id){
  return request({
    url: '/system/Interview/getInterviewById/'+id,
    method: 'get'
  })
}
