import request from '@/utils/request'
/**
* 分页查询学生信息
*/
export function listStudentForPage(query){
   return request({
     url:`/system/student/listStudentForPage`,
     method:'get',
     params:query
   })
}
/**
* 添加一个学生
*/
export function addStudent(data){
  return request({
    url:`/system/student/addStudent`,
    method:'post',
    data
  })
}

/**
* 修改一个学生
*/
export function updateStudent(data){
  return request({
    url:`/system/student/updateStudent`,
    method:'put',
    data
  })
}
/**
* 删除用户信息
*/
export function deleteStudentByIds(studentIds){
  return request({
    url:`/system/student/deleteStudentByIds/${studentIds}`,
    method:'delete',
  })
}

/**
 * 根据学生ID查询角色信息
 */
export function getStudentById(studentId){
  return request({
    url:`/system/student/getStudentById/${studentId}`,
    method:'get'
  })
}

/**
 * 根据学生ID查询角色信息
 */
export function getClassByUserCampus(){
  return request({
    url:`/system/class/getClassByUserCampus`,
    method:'get'
  })
}

export function getStudentByClassId(classId){
  return request({
    url: 'system/student/getStudentByClassId/'+classId,
    method: 'get'
  })
}

export function getAllStudent(){
  return request({
    url: 'system/student/getAllStudent',
    method: 'get'
  })
}

export function savePay(id,data){
  return request({
    url: 'system/gradeschool/savePay',
    method: 'post',
    params:{
      "StudentId":id
    },
    data
  })
}
