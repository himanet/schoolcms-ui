
import request from '@/utils/request'
/**
 * 分页查询所有班级信息
 * @param {*} query
 * @returns
 */
export function listClassForPage(query){
  return request({
    url:'/system/class/listClassForPage',
    method:'get',
    params:query
  })
}
/**
 * 添加班级信息
 * @returns
 */
export function addClass(data){
  return request({
    url:'/system/class/addClass',
    method:'put',
    data:data
  })
}
/**
 * 根据ID查询班级信息
 * @returns
 */
export function getClassById(id){
  return request({
    url:'/system/class/getClassById/'+id,
    method:'get'
  })
}
/**
 * 修改班级信息
 * @returns
 */
export function updateClass(data){
  return request({
    url:'/system/class/updateClass',
    method:'put',
    data
  })
}
/**
 * 根据ID删除班级信息
 * @returns
 */
export function deleteClassByIds(ids){
  return request({
    url:'/system/class/deleteClassByIds/'+ids,
    method:'delete'
  })

}
/**
 * 查询所有可用班级信息不分页
 * @returns
 */
export function selectAllClass(){
  return request({
    url:'/system/class/selectAllClass',
    method:'get'
  })
}

/**
 * 查询所有可用班级信息不分页
 * @returns
 */
export function getClassByUserCampus(){
  return request({
    url:'/system/class/getClassByUserCampus',
    method:'get'
  })
}

