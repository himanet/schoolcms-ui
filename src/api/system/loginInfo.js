import request from '@/utils/request'
const SERVICE_NAME = "ihis-system"

export function listForPage(query){
  return request({
    url:`/system/loginInfo/listForPage`,
    method:'get',
    params:query
  })
}
export function deleteLoginInfoByIds(ids){
  return request({
    url:`/system/loginInfo/deleteLoginInfoByIds/${ids}`,
    method:'delete',
  })
}
export function clearLoginInfo(){
  return request({
    url:`/system/loginInfo/clearLoginInfo`,
    method:'delete',
  })
}