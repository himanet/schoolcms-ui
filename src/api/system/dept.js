
import request from '@/utils/request'
const SERVICE_NAME = "ihis-system"
/**
 * 分页查询所有部门信息
 * @param {*} query 
 * @returns 
 */
export function listDeptForPage(query){
  return request({
    url:`/system/dept/listDeptForPage`,
    method:'get',
    params:query
  })
}
/**
 * 添加部门信息
 * @returns 
 */
export function addDept(data){
  return request({
    url:`/system/dept/addDept`,
    method:'post',
    params:data
  })
}
/**
 * 根据ID查询部门信息
 * @returns 
 */
export function getDeptById(id){
  return request({
    url:`/system/dept/getDeptById/${id}`,
    method:'get'
  })
}
/**
 * 修改部门信息
 * @returns 
 */
export function updateDept(data){
  return request({
    url:`/system/dept/updateDept`,
    method:'put',
    data
  })
}
/**
 * 根据ID删除部门信息
 * @returns 
 */
export function deleteDeptByIds(ids){
  return request({
    url:`/system/dept/deleteDeptByIds/${ids}`,
    method:'delete'
  })

}
/**
 * 查询所有可用部门信息不分页
 * @returns 
 */
export function selectAllDept(){
  return request({
    url:`/system/dept/selectAllDept`,
    method:'get'
  })
}