import request from '@/utils/request'
const SERVICE_NAME = "ihis-system"
/**
* 分页查询用户信息
*/
export function listUserForPage(query){
   return request({
     url:`/system/user/listUserForPage`,
     method:'get',
     params:query
   })
}
/**
* 添加用户信息
*/
export function addUser(data){
  return request({
    url:`/system/user/addUser`,
    method:'post',
    data
  })
}
/**
* 根据用户Id查询用户信息
*/
export function getUserById(userId){
  return request({
    url:`/system/user/getUserById/${userId}`,
    method:'get',
  })
}
/**
* 修改用户信息
*/
export function updateUser(data){
  return request({
    url:`/system/user/updateUser`,
    method:'put',
    params:data
  })
}
/**
* 删除用户信息
*/
export function deleteUserByIds(userIds){
  return request({
    url:`/system/user/deleteUserByIds/${userIds}`,
    method:'delete',
  })
}
/**
* 重置用户密码
*/
export function resetPwd(userIds){
  return request({
    url:`/system/user/resetPwd/${userIds}`,
    method:'put',
  })
}
/**
* 查询所有可用的用户信息
*/
export function selectAllUser(select){
  return request({
    url:`/system/user/selectAllUser`,
    method:'get',
  })
}
/**
 * 根据用户ID查询角色IDS
 * @returns
 */
export function getRoleIdsByUserId(userId){
  return request({
    url:`/system/role/getRoleIdsByUserId/${userId}`,
    method:'get',
  })
}
/**
 * 保存用户和角色之间的关系
 * @returns
 */
 export function saveUserRole(userId,roleIds){
   if(roleIds.length ===0) {
    roleIds=[-1]
   }
  return request({
    url:`system/role/saveRoleUser/${userId}/${roleIds}`,
    method:'post',
  })
}
/**
 * 查询需要排班的用户信息
 */
 export function queryUsersNeedScheduling(deptId,userId){
  if(deptId === undefined) {
    deptId=-1
   }
   if(userId === undefined) {
    userId=-1
   }
  return request({
    url:`/system/user/queryUsersNeedScheduling/${deptId}/${userId}`,
    method:'get',
  })
}
/**
 * 修改校区
 * @param userType
 * @returns {AxiosPromise}
 */
export function getUserTypeByUserId(userType){
  return request({
    url: '/system/user/getUserTypeByUserId/'+userType,
    method:'get'
  })
}

/**
 * 修改用户密码
 * @param data
 * @returns {AxiosPromise}
 */
export function updateUserPwd(data){
  return request({
    url: '/system/user/updateUserPwd',
    method: 'post',
    data:data
  })
}

/**
 * 根据角色code查询用户
 * @param data
 * @returns {AxiosPromise}
 */
export function getUserByRole(data){
  return request({
    url: '/system/user/getUserByRole/'+data,
    method: 'get',
  })
}