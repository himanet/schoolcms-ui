import request from '@/utils/request'
const SERVICE_NAME = "ihis-system"

/**
 * 分页查询角色信息
 */
export function listRoleForPage(query){
    return request({
      url:`/system/role/listRoleForPage`,
      method:'get',
      params:query
    })
}
/**
 * 添加角色信息
 */
export function addRole(data){
  return request({
      url:`/system/role/addRole`,
      method:'post',
      params:data
  })
}
/**
 * 根据角色ID查询角色信息
 */
export function getRoleById(roleId){
  return request({
    url:`/system/role/getRoleById/${roleId}`,
    method:'get'
  })
}
/**
 * 修改角色信息
 * @param {*} data 
 * @returns 
 */
export function updateRole(data){
  return request({
    url:`/system/role/updateRole`,
    method:'put',
    params:data
  })
}
/**
 * 根据ID删除角色信息
 */
 export function deleteRoleByIds(roleIds){
  return request({
    url:`/system/role/deleteRoleByIds/${roleIds}`,
    method:'delete'
  })
}
/**
 * 不分页面查询有效的
 */
export function selectAllRoles(){
  return request({
    url:`/system/role/selectAllRoles`,
    method:'get'
  })
}
/**
 * 保存角色和菜单之间的关系
 */
 export function saveRoleMenu(roleId,menuIds){
   // 处理如果没有选择菜单数据。无法匹配后台数据的问题
  if (menuIds.length === 0) {
    menuIds = [-1]
  }

  return request({
    url:`/system/role/saveRoleMenu/${roleId}/${menuIds}`,
    method:'post'
  })
}