import request from '@/utils/request'
const SERVICE_NAME = "ihis-system"
/**
 * 分页查询
 * @param {*} query 
 * @returns 
 */
export function listForPage(query){
  return request({
      url:`/system/operLog/listForPage`,
      method:'get',
      params:query
  })
}
/**
 * 根据id 删除
 * @param {*}} ids 
 * @returns 
 */
export function deleteOperLogByIds(ids){
  return request({
    url:`/system/operLog/deleteOperLogByIds/${ids}`,
    method:'delete'
  })
}
/**
 * 
 * @returns 全删除
 */
export function clearAllOperLog(){
  return request({
    url:`/system/operLog/clearAllOperLog`,
    method:'delete'
  })
}