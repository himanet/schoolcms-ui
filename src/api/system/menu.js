import request from '@/utils/request'
const SERVICE_NAME = "ihis-system"
/**
 * 查询所有菜单及权限信息
 * @param {*} query 
 * @returns 
 */
export function listAllMenus(query){
  return request({
      url:`/system/menu/listAllMenus`,
      method:'get',
      params:query
  })
}
/**
 * 查询菜单的下拉树
 * @returns 
 */
export function selectMenuTree(){
  return request({
    url:`/system/menu/selectMenuTree`
    ,method:'get'
  })
}
/**
 * 添加菜单和权限信息
 * @param {*} data 
 * @returns 
 */
export function addMenu(data){
  return request({
    url:`/system/menu/addMenu`,
    method:'post',
    params:data
  })
}
/**
 * 根据ID查询菜单和权限信息
 * @param {*} menuId 
 * @returns 
 */
export function getMenuById(menuId){
  return request({
    url:`/system/menu/getMenuById/${menuId}`,
    method:'get',
  })
}
/**
 * 修改菜单和权限信息
 * @param {*} data 
 * @returns 
 */
export function updateMenu(data){
  return request({
    url:`/system/menu/updateMenu`,
    method:'put',
    params:data
  })
}
/**
 * 根据ID删除菜单和权限信息
 * @param {*} menuId 
 * @returns 
 */
export function deleteMenuById(menuId){
  return request({
    url:`/system/menu/deleteMenuById/${menuId}`,
    method:'delete',
  })
}
/**
 * 根据角色ID查询已分配菜单ID[只查子节点]
 */
export function getMenuIdsByRoleId(roleId){
  return request({
   url:`/system/menu/getMenuIdsByRoleId/${roleId}`,
   method:'get'
  })
}