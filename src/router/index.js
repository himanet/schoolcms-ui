import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

/* Layout */
import Layout from '@/layout'
/**
 * constantRoutes 常量路由，不会改变  且常用
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: '首页',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      }
    ]
  }
]
//动态路由   由后台显示或隐藏 这些路由由后台的数据控制是否显示
export const asyncRoutes = [
  {
    path: '/system',
    component: Layout,
    redirect: 'noRedirect',
    alwaysShow: true,
    name: '/system',
    meta: {
      title: '系统管理',
      icon: 'lock'
    },
    children: [
      {
        path: 'dept',
        component: () => import('@/views/system/dept'),
        name: '/system/dept',
        meta: {
          title: '部门管理',
          icon: 'edit'
        }
      },
      {
        path: 'user',
        component: () => import('@/views/system/user'),
        name: '/system/user',
        meta: {
          title: '教师管理',
          icon: 'list'
        }
      },
      {
        path: 'role',
        component: () => import('@/views/system/role'),
        name: '/system/role',
        meta: {
          title: '角色管理',
          icon: 'tab'
        }
      },
      {
        path: 'menu',
        component: () => import('@/views/system/menu'),
        name: '/system/menu',
        meta: {
          title: '菜单管理',
          icon: 'bug'
        }
      },
      {
        path: 'dict',
        component: () => import('@/views/system/dict/type'),
        name: '/system/dict',
        meta: {
          title: '系统值集',
          icon: 'zip'
        }
      },

      {
        path: 'log_login',
        component: () => import('@/views/system/loginInfo'),
        name: '/system/log_login',
        meta: {
          title: '登录日志管理',
          icon: 'clipboard'
        }
      },
      {
        path: 'log_opt',
        component: () => import('@/views/system/operLog'),
        name: '/system/log_opt',
        meta: {
          title: '操作日志管理',
          icon: 'clipboard'
        }
      },

    ]
  },
  {
    path: '/statistics',
    component: Layout,
    redirect: 'noRedirect',
    name: '/statistics',
    meta: {
      title: '教务平台',
      icon: 'example'
    },
    children: [
      {
        path: 'class',
        component: () => import('@/views/system/class'),
        name: '/statistics/class',
        meta: { title: '班级管理', icon: 'edit' }
      },
      {
        path: 'student',
        component: () => import('@/views/system/student'),
        name: '/statistics/student',
        meta: { title: '学生管理', icon: 'list' },
        hidden: true
      },
      {
        path: 'interview',
        component: () => import('@/views/system/Interview'),
        name: '/statistics/interview',
        meta: { title: '访谈记录', icon: 'list' },
        hidden: true
      },
      {
        path: 'gotoschool',
        component: () => import('@/views/system/gotoschool'),
        name: '/statistics/gotoschool',
        meta: { title: '升学统计', icon: 'edit' }
      },
      {
        path: 'getjob',
        component: () => import('@/views/system/getjob'),
        name: '/statistics/getjob',
        meta: { title: '就业管理', icon: 'edit' }
      },
    ]
  },
 

]

//最终路由  相当于弹出层
export const lastRoute = [
  {
    path: '/dict',
    //显示在页面右侧 并添加tag
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'data/:dictId(\\d+)',
        component: () => import('@/views/system/dict/data'),
        name: '/dict',
        meta: { title: '数据值集' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]
const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  // 初始化时将所有路由都加载上，否则会出现刷新页面404的情况
  routes: constantRoutes,
  mode:'history'
})
const router = createRouter()
// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}
export default router
