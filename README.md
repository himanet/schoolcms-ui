## 简介

校企教务系统-一期

## 功能

```
- 登录 / 注销

- 系统管理
  - 部门管理
  - 教师管理
  - 角色管理
  - 菜单管理
  - 值集管理
  - 登录日志管理
  - 操作日志管理

- 教务平台
  - 班级管理
  - 学生管理
```

## 开发

```bash
# 克隆项目
git clone https://gitee.com/himanet/schoolcms-ui.git

# 进入项目目录
cd schoolcms-ui

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 其它

```bash
# 预览发布环境效果
npm run preview

# 预览发布环境效果 + 静态资源分析
npm run preview -- --report

# 代码格式检查
npm run lint

# 代码格式检查并自动修复
npm run lint -- --fix
```

## 开发人员

<p align="center">
    <a href="https://wpa.qq.com/msgrd?v=3&uin=1376901045&site=qq&menu=yes">
      <img width="320" style="float:left;border-radius:50%;overflow:hidden;" src="http://q1.qlogo.cn/g?b=qq&nk=1376901045&s=640">
    </a>
    <a href="https://wpa.qq.com/msgrd?v=3&uin=178217115&site=qq&menu=yes">
      <img width="320" style="float:left;border-radius:50%;overflow:hidden;" src="http://q1.qlogo.cn/g?b=qq&nk=178217115&s=640">
    </a>
    <a href="https://wpa.qq.com/msgrd?v=3&uin=95163552&site=qq&menu=yes">
      <img width="320" style="float:left;border-radius:50%;overflow:hidden;" src="http://q1.qlogo.cn/g?b=qq&nk=95163552&s=640">
    </a>
</p>

